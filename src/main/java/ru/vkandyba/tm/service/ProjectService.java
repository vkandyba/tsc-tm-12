package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Boolean existsByIndex(Integer index) {
        return projectRepository.existsByIndex(index);
    }

    @Override
    public Boolean existsById(String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public Project findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        if(status == null) return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        if(status == null) return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if(status == null) return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
